mod words;
mod game;


fn main() {
    let game = game::Game {word_list: words::load_words(), tries: 7};
    game.play_game();
}
