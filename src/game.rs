use std::io::stdin;
use rand::seq::SliceRandom;


pub struct Game{
    pub word_list: Vec<String>,
    pub tries: i32,
}

impl Game {

    fn take_input(&self) -> Option<char> {
        println!("Enter input: ");
        let mut input = String::new();
        stdin().read_line(&mut input).unwrap();

       match input.len() {
            2 => Some(self.filter_input(&input)),
            _ => self.take_input()
       }
    }

    fn filter_input(&self, input: &String) -> char {
        input.chars().nth(0).unwrap()
    }

    fn choose_word(&self) -> Option<String> {
        self.word_list.choose(&mut rand::thread_rng()).cloned()
    }

    fn print_current(&self, word: &String, guessed: &Vec<char>) {
        let mut message: Vec<char> = Vec::new();
        for x in word.chars(){
            match guessed.contains(&x) {
                true => message.push(x),
                false => message.push('_')
            }

        }
        print!("Current word: ");
        for character in message {
            print!("{character} ");
        }
        println!("Current guesses {:?}", String::from_iter(guessed));
        println!("")
    }

    fn is_valid_letter(&self, letter: &char) -> bool{
        letter.is_alphabetic()
    }

    fn check_letter_against_word(&self, guess: &char, word: &String, guessed: &Vec<char>) -> bool{
        if guessed.contains(guess){
            return false
        }
        match word.find(*guess) {
            Some(_) => return true,
            None =>  return false
        }
    }

    fn is_word_guessed(&self, word: &str, guessed: &Vec<char>) -> bool{
        for word_char in word.chars().collect::<Vec<char>>(){

            if !guessed.contains(&word_char) {
                return false                
            }
        }
        true
    }

    pub fn play_game(&self) {
        let mut tries = 0; 

        let word = self.choose_word().unwrap();
        let mut guessed: Vec<char> = Vec::new();
        while tries < self.tries {
            self.print_current(&word, &guessed);
            let guess = self.take_input().unwrap();
            if !self.is_valid_letter(&guess) {
                println!("Not a valid letter.");
                tries += 1;
                continue
            }
            if self.check_letter_against_word(&guess, &word, &guessed){
                guessed.push(guess);
                if self.is_word_guessed(&word, &guessed) {
                    println!("Congratulations the word was {word}");
                    return
                }
            }
            else {
                guessed.push(guess);
                tries += 1; 
                println!("Not in word!");
                continue
            }
        }
        println!("You lost. word was {word}")
    }
}

