
use std::{fs::File, io::Read};

pub fn load_words() -> Vec<String> {

    let file_open_result = File::open("words.txt");

    return match file_open_result {
        Ok(file) => build_vector(file),
        Err(_) => vec!["test".into(), "cookie".into(), "cake".into(), "drugs".into()]
    }
}

fn build_vector(mut input: File) -> Vec<String>{
    let mut buff = String::new();
    input.read_to_string(&mut buff);
    let vector: Vec<String> = buff.split("\n").map(|s| s.to_string()).collect();
    vector
}